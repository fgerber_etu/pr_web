@echo off

rem Ouvrir un nouveau terminal et lancer la commande Flask
start cmd /k "flask run"

rem Attendre un court instant pour que le serveur Flask démarre
timeout /t 2

rem Ouvrir un nouveau terminal, se déplacer dans le répertoire frontend et lancer la commande npm run serve
start cmd /k "cd frontend && npm run serve"
