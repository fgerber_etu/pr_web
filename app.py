from flask import Flask, request, jsonify
from flask_mysqldb import MySQL
from flask_cors import CORS

app = Flask(__name__)
# Pour pouvoir accepter des requetes depuis la même addresse
CORS(app)

# Configuration de la base de données
app.config['MYSQL_HOST'] = 'benoit.darties.fr'
app.config['MYSQL_USER'] = 'elsa.besse'
app.config['MYSQL_PASSWORD'] = 'TMs/75p6ej6VXGVH'
app.config['MYSQL_DB'] = 'skymarket'

mysql = MySQL(app)

@app.route('/')
def index():
    return "Bienvenue sur l'API du site de vente d'avions!"

@app.route('/annonces', methods=['GET'])
def get_annonces():
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM aircrafts''')
    rv = cur.fetchall()
    return jsonify(rv)

@app.route('/users', methods=['GET'])
def get_users():
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM users''')
    rv = cur.fetchall()
    return jsonify(rv)

def get_user_by_name(username):
    cur = mysql.connection.cursor()
    cur.execute("SELECT id FROM users WHERE username = %s", (username,))
    user = cur.fetchone()
    cur.close()
    return user if user else None

@app.route('/user/<int:user_id>', methods=['GET'])
def get_user_by_id(user_id):
    try:
        cur = mysql.connection.cursor()

        cur.execute("SELECT username, email FROM users WHERE id = %s", (user_id,))
        user = cur.fetchone()

        cur.close()

        if user:
            return jsonify(user), 200
        else:
            return jsonify({'message': 'Utilisateur non trouvé'}), 404

    except Exception as e:
        return jsonify({'message': 'Erreur lors de la récupération des informations de contact du propriétaire', 'error': str(e)}), 500

@app.route('/create_annonce', methods=['POST'])
def create_annonce():
    data = request.json
    username = data.get('token')
    user = get_user_by_name(username)
    
    if not user:
        return jsonify({'error': 'User not found'}), 404
    
    price = data.get('price')
    year = data.get('year')
    manufacturer = data.get('manufacturer')
    model = data.get('model')
    registration = data.get('registration')
    serial_number = data.get('serial_number')
    condition = data.get('condition')
    seats = data.get('seats')
    description = data.get('description')
    print(data)
    cur = mysql.connection.cursor()
    cur.execute('''INSERT INTO aircrafts (user_id, price, year, manufacturer, model, registration, serial_number, aircraft_condition, seats, description)
                   VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)''',
                (user[0], price, year, manufacturer, model, registration, serial_number, condition, seats, description))
    mysql.connection.commit()
    cur.close()

    return jsonify({'message': 'Annonce created successfully!'}), 201

@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')

    cur = mysql.connection.cursor()
    query = "SELECT * FROM users WHERE username = %s AND password = %s"
    cur.execute(query, (username, password))
    user = cur.fetchone()
    cur.close()

    if user:
        # Assuming token is just the username for simplicity
        token = username
        response = jsonify({"token": token})
        response.status_code = 200
        return response
    else:
        response = jsonify({"error": "Invalid username or password"})
        response.status_code = 401
        return response

@app.route('/register', methods=['POST'])
def register():
    try:
        username = request.json.get('username')
        email = request.json.get('email')
        password = request.json.get('password')

        cur = mysql.connection.cursor()

        cur.execute("SELECT * FROM users WHERE username = %s", (username,))
        user = cur.fetchone()
        if user:
            return jsonify({'message': 'Nom d\'utilisateur déjà utilisé. Veuillez en choisir un autre.'}), 400

        cur.execute("INSERT INTO users (username, email, password) VALUES (%s, %s, %s)", (username, email, password))
        mysql.connection.commit()
        cur.close()

        return jsonify({'message': 'Inscription réussie'}), 201

    except Exception as e:
        return jsonify({'message': 'Erreur lors de l\'inscription', 'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
