@echo off

rem Installer les dépendances Python
pip install -r requirements.txt

rem Se déplacer dans le dossier frontend
cd frontend

rem Installer les dépendances Node.js
npm install
