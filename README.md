# Application web

## Installation des dépendences

> :warning: **`pip` et `node.js` doivent être installés pour exécuter les scripts**

Exécuter le fichier `install_dep.bat` dans le répertoire racine du projet

## Lancement du site

Exécuter le fichier `launch_website.bat`dans le répertoire racine du projet
